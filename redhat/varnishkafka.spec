%undefine _strict_symbol_defs_build
%global commit a0ef8fcd142fb3db46cb8e2c386d9b4776aecc75
%global gittag 1.0.15
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Summary:       varnishkafka is a varnish log collector with an integrated Apache Kafka producer. It was written from scratch with performance and modularity in mind.
Name:          varnishkafka
Release:       2
Version:       1.0.15
License:       2-Clause BSD 2-Clause
URL:           https://gitlab.wikimedia.org/repos/sre/varnishkafka
Source0:       https://gitlab.wikimedia.org/repos/sre/%{name}/-/archive/%{commit}/%{name}-%{shortcommit}.tar.gz

BuildRequires: varnish-devel
BuildRequires: librdkafka-devel
buildRequires: yajl-devel

%description
varnishkafka is a varnish log collector with an integrated Apache Kafka producer. It was written from scratch with performance and modularity in mind.

%prep
%autosetup -n varnishkafka-%{commit}

cat > varnishkafka.service << 'EOF'
[Unit]
Description=manage varnishkafka
After=network-online.target
Wants=network-online.target systemd-networkd-wait-online.service

[Service]
Restart=no
ExecStart=/bin/sh -c '/usr/bin/%{name} -g "vxid" | tee'

[Install]
WantedBy=multi-user.target
EOF

%build
cd %{_builddir}/varnishkafka-%{commit}/
VER=%{version} make



%install
install -D %{_builddir}/varnishkafka-%{commit}/%{name}.conf.example %{buildroot}/etc/%{name}.conf
install -D %{_builddir}/varnishkafka-%{commit}/%{name} %{buildroot}/usr/bin/%{name}
install -D %{_builddir}/varnishkafka-%{commit}/%{name}.service %{buildroot}/etc/systemd/system/%{name}.service


%files
%config(noreplace) /etc/%{name}.conf
/etc/systemd/system/%{name}.service
/usr/bin/%{name}

%post
systemctl daemon-reload

%changelog
* Thu Nov 16 2023 Julien Godin<julien.godin@camptocamp.com> 1.0
* First package creation


